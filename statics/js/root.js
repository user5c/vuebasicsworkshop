var app = new Vue({
    el: '#listApp',
    data() {
        return {
            objetos: [{ nombre: 'objeto1', precio: 200, cant:0 }, { nombre: 'objeto2', precio: 500, cant:0 }, { nombre: 'objeto3', precio: 300, cant:0 }],
            objetosEnCarrito: [],
            total: 0
        }
    },
    computed: {
        precioTotal() {
            total = 0;
            if (this.objetosEnCarrito.length > 0) {
                this.objetosEnCarrito.forEach(element => {
                    total += element.precio;
                });
            }
            return total;
        },
    },
    methods:{
        comprar(id){
            var num_en_car = this.objetos[id]
            this.objetos[id].cant = this.objetos[id].cant + 1
            this.objetosEnCarrito.push(num_en_car)
            this.total = this.objetosEnCarrito.length
            
        },
    }
})